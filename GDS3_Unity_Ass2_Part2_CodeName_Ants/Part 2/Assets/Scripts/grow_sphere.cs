﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grow_sphere : MonoBehaviour {
    // Use this for initialization
    void Start () {
        transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }
    void Update()
    {
        if (grow_parent.level == 1)
        {
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        if (grow_parent.level == 2)
        {
            transform.localScale = new Vector3(0.8f,0.8f,0.8f);
        }
        if (grow_parent.level == 3)
        {
            transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        }
        if (grow_parent.level == 4)
        {
            transform.localScale = new Vector3(1.7f, 1.7f, 1.7f);
        }
        if (grow_parent.level == 5)
        {
            transform.localScale = new Vector3(2.2f, 2.2f, 2.2f);
        }
        if (grow_parent.level == 6)
        {
            transform.localScale = new Vector3(2.8f, 2.8f, 2.8f);
        }
    }
}
