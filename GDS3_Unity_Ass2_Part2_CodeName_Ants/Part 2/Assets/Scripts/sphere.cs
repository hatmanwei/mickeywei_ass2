﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sphere : MonoBehaviour {

	private bool can_jump = true;
    public Rigidbody rb;
    private float velo;
    public AudioSource move;
	void OnCollisionStay(Collision other) {
		can_jump = true;
	}
	
	void OnCollisionExit(Collision other) {
		can_jump = false;
	}
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKeyDown("space") && (can_jump == true)){
			rb.AddForce(0,300,0,ForceMode.Force);
        }
        velo = (Mathf.Sqrt(Mathf.Pow(rb.velocity.x, 2) + Mathf.Pow(rb.velocity.z, 2)) / 2);
        move.pitch = velo;
        move.volume = velo/2;

        if (this.transform.position.y < -9)
        {
            this.transform.position = new Vector3(9, 3, 15);
        }
    }
}
