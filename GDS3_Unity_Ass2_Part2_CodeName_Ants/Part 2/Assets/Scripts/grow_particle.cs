﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grow_particle : MonoBehaviour {
    private float radius = 1;
    private float rate = 0;

    // Use this for initialization
    void Start () {
        radius = 1;
        rate = 0;
    }
            // Update is called once per frame
    void Update () {
        Debug.Log(grow_parent.level);
        if (grow_parent.level == 1)
        {
            radius = 0.3f;
            rate = 300;
        }
        else if (grow_parent.level == 2)
        {
            radius = 0.5f;
            rate = 600;
        }
        else if (grow_parent.level == 3)
        {
            radius = 0.9f;
            rate = 1200;
        }
        else if (grow_parent.level == 4)
        {
            radius = 1.1f;
            rate = 2400;
        }
        else if (grow_parent.level == 5)
        {
            radius = 1.3f;
            rate = 4800;
        }
        else if (grow_parent.level == 6)
        {
            radius = 1.6f;
            rate = 9600;
        }
        ParticleSystem ps = GetComponent<ParticleSystem>();
        var sh = ps.shape;
        var em = ps.emission;
        sh.radius = radius;
        em.rateOverTime = rate;
    }
}
