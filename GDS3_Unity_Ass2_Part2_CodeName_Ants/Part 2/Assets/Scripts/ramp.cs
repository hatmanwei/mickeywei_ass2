﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ramp : MonoBehaviour {
    public GameObject RAMP;
    public GameObject RAMP2;
    public Rigidbody rb;
    public GameObject honey;
    private bool isHoney;
    private bool can_jump = true;
	// Use this for initialization
	void Start () {
        RAMP2.SetActive(false);
        RAMP.SetActive(true);
        can_jump = true;
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnCollisionEnter(Collision other)
    {
        
        if (this.name == "ramp1")
        {
            RAMP2.SetActive(true);
            RAMP.SetActive(false);
            rb.AddForce(-300, 400, 0, ForceMode.Force);
        } else if (this.name == "ramp2")
        {
            rb.AddForce(300, 400, 0, ForceMode.Force);
            RAMP2.SetActive(false);
        }
    }

    
}
