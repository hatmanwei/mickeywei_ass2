﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class honey : MonoBehaviour {
    public GameObject plane;
    public GameObject core;
    public GameObject sphere;
    public bool isHoney = false;

	// Use this for initialization
	void Start () {
        isHoney = false;
        plane.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            isHoney = true;
        }
    }
    // Update is called once per frame
    void Update () {
		if (isHoney == true)
        {
            plane.SetActive (true);
            Renderer coreRend = core.GetComponent<Renderer>();
            Renderer sphereRend = sphere.GetComponent<Renderer>();
            coreRend.material.SetColor("_Color", Color.yellow);
            sphereRend.material.SetColor("_Color", Color.yellow);
        }
	}
}
