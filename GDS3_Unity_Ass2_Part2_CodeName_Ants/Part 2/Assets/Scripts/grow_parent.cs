﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grow_parent : MonoBehaviour {
    public static int level = 1;
    public float radius = 0.4f;

    private void Start()
    {
        level = 1;
        radius = 0.4f;
    }
    // Use this for initialization
    void Update() {
        SphereCollider collider = GetComponent<SphereCollider>();
        collider.radius = radius;
    }
    void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.name == "cherry") && (level == 1))
        {
            level = 2;
            radius = 0.5f;
        }
        else if ((other.gameObject.name == "Candy") && (level == 2))
        {
            level = 3;
            radius = 0.9f;
        }
        else if ((other.gameObject.name == "tomato") && (level == 3))
        {
            level = 4;
            radius = 1.1f;
        }
        else if ((other.gameObject.name == "watermelon") && (level == 4))
        {
            level = 5;
            radius = 1.3f;
        }
        else if ((other.gameObject.name == "waffle") && (level == 5))
        {
            level = 6;
            radius = 1.6f;
        }
    }
}
