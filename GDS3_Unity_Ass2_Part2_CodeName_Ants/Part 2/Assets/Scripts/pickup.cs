﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pickup : MonoBehaviour {
	public GameObject targetObject;
	private Vector3 target;
	private bool pickedUp = false;
    public Transform sphere;
    public GameObject splash;
    public AudioSource munch;
    public AudioSource denied;
    void Start() {
		target = transform.position;
        pickedUp = false;
	}
	void Update() 
	{
		if (pickedUp) {
			target = targetObject.transform.position;
		} else
        {
            target = transform.position;
        }
		transform.position = target;
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.name == "Player") {
            if ((this.gameObject.name == "cherry") && (grow_parent.level == 1))
            {
                munch.Play();
                Destroy(splash);
                pickedUp = true;
                GetComponent<Collider>().enabled = false;
                this.transform.SetParent(sphere);
            } else if ((this.gameObject.name == "Candy") && (grow_parent.level == 2)) {
                munch.Play();
                Destroy(splash);
                pickedUp = true;
                GetComponent<Collider>().enabled = false;
                this.transform.SetParent(sphere);
            }
            else if ((this.gameObject.name == "tomato") && (grow_parent.level == 3))
            {
                munch.Play();
                Destroy(splash);
                pickedUp = true;
                GetComponent<Collider>().enabled = false;
                this.transform.SetParent(sphere);
            }
            else if ((this.gameObject.name == "watermelon") && (grow_parent.level == 4))
            {
                munch.Play();
                Destroy(splash);
                pickedUp = true;
                GetComponent<Collider>().enabled = false;
                this.transform.SetParent(sphere);
            }
            else if ((this.gameObject.name == "waffle") && (grow_parent.level == 5))
            {
                munch.Play();
                Destroy(splash);
                pickedUp = true;
                GetComponent<Collider>().enabled = false;
                this.transform.SetParent(sphere);
            }
            else if ((this.gameObject.name == "cake") && (grow_parent.level == 6))
            {
                munch.Play();
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                denied.Play();
            }
        }
    }
}
