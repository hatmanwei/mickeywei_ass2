﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilt : MonoBehaviour {
    public GameObject player;
    private Vector3 tiltOffset;
	public Transform cam;
	private Vector3 moveHorizontal, moveVertical;
    private float factor = 0.5f;
    private float hypo;
    private float forwardModifier = 0;
    void Start () {
        factor = 0.5f;
        forwardModifier = 0;
    }

	void Update () {
        tiltOffset = new Vector3(Mathf.Abs(player.transform.position.x), Mathf.Abs(player.transform.position.y), Mathf.Abs(player.transform.position.z));
        hypo = Mathf.Sqrt(Mathf.Pow(tiltOffset.x, 2) + Mathf.Pow(tiltOffset.z, 2));
        //moveHorizontal = Input.GetAxis ("Horizontal") * cam.forward;

        if ((Input.GetKey("w")) && (forwardModifier < 1))
        {
            forwardModifier += 0.1f;
        } else
        {
            if (forwardModifier > 0)
            {
                forwardModifier -= 0.1f;
            }
        }
        moveVertical = forwardModifier * cam.right;
        //moveVertical = Input.GetKey ("W") * cam.right;
        factor = 2f / (hypo / 2);
        factor = Mathf.Clamp(factor, 0.2f, 0.8f);
		transform.Rotate ((moveVertical/* - moveHorizontal*/) * factor);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,0,0), 0.05f);
        Debug.Log(factor);
		/*
		float moveHorizontal = Input.GetAxis ("Horizontal") * camera.right;
		float moveVertical = Input.GetAxis ("Vertical") * camera.forward;

		transform.rotation *= Quaternion.Euler(moveVertical, 0, -moveHorizontal);
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,0,0), 0.1f);*/
    }
}
