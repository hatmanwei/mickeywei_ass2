﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class level_change : MonoBehaviour {
    private int currentLevel = 1;
	public GameObject level1;
	public GameObject level2;
	public GameObject level3;
	public GameObject level4;

	public GameObject sphere;
	public Rigidbody sphererb;

	void Start () {
		level2.SetActive (true);
	}
    void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.name == "Sphere")
            {
                Debug.Log(currentLevel);
                if (currentLevel == 1) //switch to level 2
                {
					sphere.transform.position = new Vector3 (-1.35f,11f,-5f);//10,5.5,8.
					this.transform.position = new Vector3 (-2.5f,-5f,-4f);
					level1.SetActive (false);
					level2.SetActive (true);
                    currentLevel ++;
                }
				else if (currentLevel == 2) //switch to level 3
                {
					sphere.transform.position = new Vector3 (-7.75f,7.5f,-9.7f);
					this.transform.position = new Vector3 (4.2f,-5f,-17f);
					level2.SetActive (false);
					level3.SetActive (true);
                    currentLevel ++;
                }
				else if (currentLevel == 3) //switch to level 4
                {
					sphere.transform.position = new Vector3 (0f,6f,-8f);
					this.transform.position = new Vector3 (4.2f,-5f,-17f);
					level3.SetActive (false);
					level4.SetActive (true);
					currentLevel++;
                }
				else if (currentLevel == 4) //switch to level 1
				{
					sphere.transform.position = new Vector3 (-5.3f,6.3f,-11.8f);
					this.transform.position = new Vector3 (-16.5f, -7.5f, -8.5f);
					level4.SetActive (false);
					level1.SetActive (true);
					currentLevel = 1;
				}
                Debug.Log(currentLevel);
        }
    }

    void Update () {
		if (sphere.transform.position.y < -25) 
		{
			sphere.transform.position = new Vector3 (-1.35f,11f,-5f);
			sphererb.velocity = Vector3.zero;
		}
	}
}