﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_controller : MonoBehaviour
{
    public Transform player;
    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float camLerp = 0.2f;
    [Range(1.0f, 5.0f)]
    public float rotationSpeed = 1.0f;

    void Start()
    {
        _cameraOffset = transform.position - player.position;
    }

    private void Update()
    {
        Quaternion camTurnAngle_x = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationSpeed, Vector3.up);

        _cameraOffset = camTurnAngle_x * _cameraOffset;
    }
    void LateUpdate()
    {
        Vector3 newPos = new Vector3(player.position.x + _cameraOffset.x, player.position.y + _cameraOffset.y, player.position.z + _cameraOffset.z);
        transform.position = Vector3.Slerp(transform.position, newPos, camLerp);

        transform.LookAt(player.position);
    }
}